using System;
using System.Numerics;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace calc_s
{
	public class MainCalc
	{
		protected static int SS = 22;
		protected static string alphabet = "0123456789ABCDEFGHIJKL";

		public MainCalc () {

		}

		public string inputVar(string number, string message){
			number = "";
          while(true){

              bool iinfo = false;
				ConsoleKeyInfo keyInfo = Console.ReadKey ();
				if (keyInfo.Key == ConsoleKey.Escape) {
					Environment.Exit (0);
				} else if (keyInfo.Key == ConsoleKey.Enter) {
					break;
                }
                else if (keyInfo.Key == ConsoleKey.F1) {
                    Console.WriteLine("Справка: Калькулятор для деления с остатком 22-ричных чисел, для получения результата введите делимое и делитель." + 
                    "Результат представляет собой целую часть полученного числа и остаток от деления. \n\r Выполнили студенты группы 1374 \n\r Сапроненков М.А \n\r Морозов Ю. \n\r Муренин И. \n\r");


                    
                    Console.ReadKey();
                    iinfo = true;
                }

                else
                {
                   if(!iinfo) number += keyInfo.KeyChar;
                }
			}
            Console.WriteLine(number);

			if ((tryInput (number) == false)) {
				Console.WriteLine ("Ошибка ввода.  Возможно вы превысили 32 знака или используете строчные буквы." +
					"Попробуйте еще раз.");
				Console.WriteLine (message);
				return inputVar (number, message);
			}
			return number;
		}


		public void start() {
		
			string a = "";
			string b = "";

				a = "";
				Console.WriteLine ("Введите число a ( "+SS+" СС)");
				a = inputVar (a, "Введите a: ( "+SS+" СС)");
      


				b = "";
				Console.WriteLine ("Введите число b ( " + SS + " СС)");
				b = inputVar (b, "Введите b: ( "+SS+" СС)");	
                if (a.Length<b.Length){
                    a = "";
                    b = "";
              
                }

				Console.WriteLine ("Ответ " + division(a, b,22).Key+" , остаток - "+(division(a, b,22).Value));

		}


		private int getNumber(char c) {
			return alphabet.IndexOf (c);
		}
			
		private char getChar(int i) {
			if ( i >= alphabet.Length ) 
				return ' ';
			
			return alphabet [i];
		}


		private bool tryInput(string strIn){
		
			return Regex.IsMatch (strIn, @"^[0-9A-L]{1,32}$");

		}

        public KeyValuePair<string,string> division(string a, string b, int notation)
        {            BigInteger a_dec = ToBase10(a, notation);
            BigInteger b_dec = ToBase10(b, notation);
            BigInteger result_dec=0;
            BigInteger result_remainder=0;
            if(b_dec!=0) result_dec = a_dec / b_dec;
            if(b_dec!=0)result_remainder = a_dec % b_dec;
           
          //  BigInteger result_remainder = (BigInteger)(((a_dec % b_dec) / b_dec) * quantity);

           

            string rs1 = FromBase10(result_dec, notation);
            string rs2 =  FromBase10(result_remainder, notation);
            
            KeyValuePair<string,string> result=new KeyValuePair<string,string>(rs1,rs2); 
            return result;
        
        }

		public string plus(string a, string b) {
			if (a.Length > b.Length) {
				b = addNol (a, b); 
			} else {
				a = addNol (b, a);
			}
			int ostatok = 0;
			int n = a.Length;
			string result = "";
			for (int i = 0; i < a.Length; i++) {
				int r = getNumber (a [n - 1 - i]) + getNumber (b [n - i - 1]);
				r += ostatok;
				if (r >= 30) {
					ostatok = 1;
					r = r - 30;
				} else {
					ostatok = 0;
				}
				result = getChar (r) + result;
			}
			return result;
		}


        public static BigInteger ToBase10(string number, int start_base)
        {
            if (start_base < 2 || start_base > 36) return 0;
            if (start_base == 10) return Convert.ToInt32(number);

            char[] chrs = number.ToCharArray();
            int m = chrs.Length - 1;
            int n = start_base;
            int x;
            BigInteger rtn = 0;

            foreach (char c in chrs)
            {

                if (char.IsNumber(c))
                    x = int.Parse(c.ToString());
                else
                    x = Convert.ToInt32(c) - 55;

                rtn += x * (Convert.ToInt32(Math.Pow(n, m)));
                m--;
            }
            return rtn;
        }

        public static string FromBase10(BigInteger number, int target_base)
        {
            if (target_base < 2 || target_base > 36) return "";
            if (target_base == 10) return number.ToString();

            int n = target_base;
            BigInteger q = number;
            int r;
            string rtn = "";

            while (q >= n)
            {
                r = (int)(q % n);
                q = q / n;

                if (r < 10)
                    rtn = r.ToString() + rtn;
                else
                    rtn = Convert.ToChar(r + 55).ToString() + rtn;
            }
            int q_small = (int)q;

            if (q < 10)
                rtn = q_small.ToString() + rtn;
            else
                rtn = Convert.ToChar(q_small + 55).ToString() + rtn;

            return rtn;
        }
 

		/// <summary>
		/// Добавление нулей к числу, у которого меньше разрядов
		/// </summary>
		/// <returns>число с добавленными нулями</returns>
		/// <param name="a">число до количества разрядов которого надо добавить</param>
		/// <param name="b">число, которому добавляют</param>
		private string addNol(string a, string b) {
			int k = Math.Abs(a.Length - b.Length);
			for (int i = 0; i < k; i++) {
				b = "0" + b; 
			}
			return b;
		}
	}
}

