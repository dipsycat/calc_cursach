﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace calc_s
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Title = "Калькулятор деления в 22 СС";

            TestCase t = new TestCase();
            t.startTest();
            
            ConsoleKeyInfo cki;

            MainCalc calc = new MainCalc();

            Console.WriteLine("Калькулятор ( " + 22 + " СС)");
            do
            {
                calc.start();
                cki = Console.ReadKey();

            } while (cki.Key != ConsoleKey.Escape);
        }
    }
}
